#!/bin/sh

bat_status=$(</sys/class/power_supply/BAT0/status) #Current status of battery 0.

# If the state of battery 0 is unknown (I.E it is not being charged / discharged),
# use the status of battery 1 instead.
if [ "$bat_status" == "Unknown" ]
then
	bat_status=$(</sys/class/power_supply/BAT1/status)
fi

#Get the capacity of both batteries.
bat0=$(</sys/class/power_supply/BAT0/capacity)
bat1=$(</sys/class/power_supply/BAT1/capacity)

((total = ($bat1 + $bat0) / 2)) #Find the total charge.

echo $total"%" $bat_status
