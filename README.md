# Simplistic battery level script for PC’s with dual batteries

This is an extremely simple script that echoes the current charge along with the battery status.
There are many scripts out there that supply battery charge level and status. However I have not been able to find any that work for laptops with two batteries (for example my ThinkPad T450).

## Running the script in the tmux status bar
To run the script in the tmux status bar simply add the following line to your .tmux.conf:
```
$ set -g status-right '#(/home/path/to/script/battery.sh)'
```

![Example of battery being displayed in tmux](http://i.imgur.com/qg9OVwr.png)

Example of battery infomation being displayed in tmux status bar.
